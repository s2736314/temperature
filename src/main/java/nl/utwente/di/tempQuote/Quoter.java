package nl.utwente.di.tempQuote;

public class Quoter {
    public double getTemperatureFahr(String celString) {
        double celsius = Double.parseDouble(celString);
        double Fahrenheit = 1.8 * celsius + 32;
        return Fahrenheit;
    }
}
